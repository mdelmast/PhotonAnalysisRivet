// PHOTONS

double bin_mgg[] = { 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 120, 130, 140, 150, 170, 200, 240, 300, 400, 600, 1000 }; //Zuzana's binning
//double bin_mgg[] = {60.,70.,80.,90.,100.,110.,120.,130.,140.,150.,170.,200.,240.,300.,400.,600.,1000.};
//double bin_mgg[] = {60.,65.,70.,75.,80.,85.,90.,95.,100.,105.,110.,120.,130.,140.,150.,170.,200.,240.,300.,400.,600.,1000.};
const int n_bin_mgg = sizeof(bin_mgg)/sizeof(double);

double bin_pTgg[] = {0.,10.,20.,25.,30.,35.,40.,45.,50.,55.,60.,65.,70.,75.,80.,90.,100.,125.,150.,200.,300.,500.};
const int n_bin_pTgg = sizeof(bin_pTgg)/sizeof(double);

double bin_deltaphi[] =  {0.,0.25,0.5,.75,1.,1.25,1.5,1.625,1.75,1.875,2.,2.125,2.25,2.375,2.5,2.575,2.65,2.725,2.8,2.85,2.9,2.95,3.,3.05,3.1,3.1416};
const int n_bin_deltaphi = sizeof(bin_deltaphi)/sizeof(double);

double bin_costhetastar[] = {0.,0.04,0.08,.12,.16,.2,.24,.28,.32,.36,.40,.44,.48,.52,.56,.60,.64,.68,.72,.76,.8,.84,.88,.92,.96,1.};
const int n_bin_costhetastar = sizeof(bin_costhetastar)/sizeof(double);

//double bin_delta_eta[] =  {0.,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,2.,2.2,2.4,2.8,3.2,3.6,4.,5.};
//const int n_bin_delta_eta = sizeof(bin_delta_eta)/sizeof(double);

// JETS

double bin_ptj1[] = {25.,30.,35.,40.,45.,50.,55.,60.,65.,70.,75.,80.,90.,105.,125.,150.,200.,275.,400,800.};
//double bin_ptj1[] = {25.,40.,50.,60.,65.,70.,75.,80.,90.,100.,110.,120.,130.,150.,200.,250.,350.,400,800.};
const int n_bin_ptj1 = sizeof(bin_ptj1)/sizeof(double);

double bin_ptj2[] = {25.,30.,35.,40.,45.,50.,55.,60.,65.,70.,75.,80.,90.,105.,125.,150.,200.,275.,400.}; 
//double bin_ptj2[] = {25.,30.,35.,40.,45.,50.,55.,60.,65.,70.,75.,80.,90.,100.,125.,150.,200.,275.,400.,600.};
const int n_bin_ptj2 = sizeof(bin_ptj2)/sizeof(double);

double bin_deltaphi_jj[] =  {0.,0.3,.6,.9,1.2,1.5,1.8,2.,2.2,2.4,2.5,2.6,2.7,2.8,2.9,3.,3.05,3.1,3.1416};
const int n_bin_deltaphi_jj = sizeof(bin_deltaphi_jj)/sizeof(double);

double bin_mass_jj[] = {0., 30., 40., 50., 60., 70., 85., 100., 115., 130., 150., 175., 200., 225., 250., 275., 300., 350., 400., 500., 600., 800., 1000., 2000.,}; 
//double bin_mass_jj[] = {0.,30.,40.,50.,60.,70.,80.,90.,100.,110.,120.,130.,140.,150.,160.,170.,180.,190.,200.,225.,250.,275.,300.,350.,400.,500.,600.,800.,1000.,2000};
const int n_bin_mass_jj = sizeof(bin_mass_jj)/sizeof(double);

double bin_deltay_jj[] = {0.,0.2,0.4,0.6,0.8,1.,1.2,1.4,1.6,1.8,2.,2.2,2.5,3.,3.5,4.5,6.};
const int n_bin_deltay_jj = sizeof(bin_deltay_jj)/sizeof(double);

//double bin_pt_jj[] = {0.,5.,10.,15.,20.,25.,30.,35.,40.,45.,50.,55.,60.,65,70.,75.,80.,90.,100.,110.,125.,150.,200.,260.,330.,400.,500.,600.};
double bin_pt_jj[] = {0., 10., 20., 30., 45., 60., 75., 90., 115., 130., 150., 200., 260., 330., 400., 500., 600.};
const int n_bin_pt_jj = sizeof(bin_pt_jj)/sizeof(double);

// PHOTON-JET

//double bin_dRgj[] = {0.6,1.,1.4,1.8,2.,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.,3.1,3.2,3.4,3.6,3.8,4.,4.5,6.};
double bin_dRgj[] = {0.6,1.,1.4,1.8,2.,2.2,2.4,2.5,2.6,2.7,2.8,2.9,3.,3.1,3.2,3.4,3.6,3.8,4.,4.5,6.};
const int n_bin_dRgj = sizeof(bin_dRgj)/sizeof(double);

double bin_dS[] = {0.,0.5,1.,1.5,1.75,2.,2.25,2.35,2.45,2.55,2.65,2.7,2.75,2.8,2.85,2.9,2.95,3.,3.05,3.1,3.1416};
const int n_bin_dS = sizeof(bin_dS)/sizeof(double);

double bin_dptggjj[] = {-300.,-100.,-40.,-20.,-10.,0.,10.,20.,40.,100.,300.};
const int n_bin_dptggjj = sizeof(bin_dptggjj)/sizeof(double);

//double bin_abs_dptggjj[] = {0.,5.,10.,15.,20.,25.,30.,40.,60.,90.,150.,300.};
//const int n_bin_abs_dptggjj = sizeof(bin_dptggjj)/sizeof(double);


