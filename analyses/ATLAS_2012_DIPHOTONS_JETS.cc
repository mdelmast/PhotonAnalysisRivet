// -*- C++ -*-
#include <iostream>
#include <sstream>
#include <string>

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/LeadingParticlesFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"

#include "Rivet/Jet.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Math/Vector3.hh"

#include "fastjet/internal/base.hh"
#include "fastjet/JetDefinition.hh"
#include "fastjet/AreaDefinition.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/PseudoJet.hh"

#include "binning.h"

namespace Rivet {

  // @brief Measurement of isolated diphoton + jets differential cross-sections
  // @author Marco Delmastro
  
  class ATLAS_2012_DIPHOTONS_JETS : public Analysis {
  public:

    // Constructor
    ATLAS_2012_DIPHOTONS_JETS()
      : Analysis("ATLAS_2012_DIPHOTONS_JETS")
    {
      _eta_bins_areaoffset += 0.0, 1.5, 3.0;
    }

  public:

    // Book histograms and initialise projections before the run
    void init() {

      FinalState fs;
      addProjection(fs, "FS");

      // FastJets for ED  correction
      FastJets fj(fs, FastJets::KT, 0.5);
      _area_def = new fastjet::AreaDefinition(fastjet::VoronoiAreaSpec());
      fj.useJetArea(_area_def);
      addProjection(fj, "KtJetsD05");

      // @NOTE: Rivet *FinalState projections will select among all particles with status==1, no matter whether they are "prompt" or "from hadron decays". Add selection later...
      IdentifiedFinalState photonfs(-2.37,2.37,22.*GeV); // etamin, etamax, ptmin 
      photonfs.acceptId(PID::PHOTON);
      addProjection(photonfs, "Photon");

      // FS excluding  photons
      VetoedFinalState vfs(fs);
      vfs.addVetoOnThisFinalState(photonfs);
      addProjection(vfs, "JetFS");

      // Jets from vetoed FS
      FastJets jetpro(vfs, FastJets::ANTIKT, 0.4);
      jetpro.useInvisibles();
      addProjection(jetpro, "Jets");

      // explicit histogram definition, since I'm not reading the results from data yet
      _h_njet           = bookHisto1D(	"_h_njet", 7, 0., 7. );

      std::vector<double> _bin_mgg		(n_bin_mgg		)	; for ( int i=0	; i< n_bin_mgg		 ; i++ ) _bin_mgg[i]		= bin_mgg[i]	      ;
      std::vector<double> _bin_pTgg		(n_bin_pTgg		)	; for ( int i=0	; i< n_bin_pTgg		 ; i++ ) _bin_pTgg[i]		= bin_pTgg[i]	      ;
      std::vector<double> _bin_deltaphi		(n_bin_deltaphi	        )	; for ( int i=0	; i< n_bin_deltaphi	 ; i++ ) _bin_deltaphi[i]	= bin_deltaphi[i]     ;
      std::vector<double> _bin_costhetastar	(n_bin_costhetastar	)	; for ( int i=0	; i< n_bin_costhetastar	 ; i++ ) _bin_costhetastar[i]	= bin_costhetastar[i] ;
      std::vector<double> _bin_ptj1		(n_bin_ptj1		)	; for ( int i=0	; i< n_bin_ptj1		 ; i++ ) _bin_ptj1[i]		= bin_ptj1[i]	      ;
      std::vector<double> _bin_ptj2		(n_bin_ptj2		)	; for ( int i=0	; i< n_bin_ptj2		 ; i++ ) _bin_ptj2[i]		= bin_ptj2[i]	      ;
      std::vector<double> _bin_deltaphi_jj	(n_bin_deltaphi_jj	)	; for ( int i=0	; i< n_bin_deltaphi_jj	 ; i++ ) _bin_deltaphi_jj[i]	= bin_deltaphi_jj[i]  ;
      std::vector<double> _bin_mass_jj		(n_bin_mass_jj	        )	; for ( int i=0	; i< n_bin_mass_jj	 ; i++ ) _bin_mass_jj[i]	= bin_mass_jj[i]      ;
      std::vector<double> _bin_deltay_jj	(n_bin_deltay_jj	)	; for ( int i=0	; i< n_bin_deltay_jj	 ; i++ ) _bin_deltay_jj[i]	= bin_deltay_jj[i]    ;
      std::vector<double> _bin_pt_jj		(n_bin_pt_jj	        )	; for ( int i=0	; i< n_bin_pt_jj	 ; i++ ) _bin_pt_jj[i]		= bin_pt_jj[i]	      ;
      std::vector<double> _bin_dRgj		(n_bin_dRgj		)       ; for ( int i=0	; i< n_bin_dRgj		 ; i++ ) _bin_dRgj[i]		= bin_dRgj[i]	      ;
      std::vector<double> _bin_dS		(n_bin_dS		)	; for ( int i=0	; i< n_bin_dS		 ; i++ ) _bin_dS[i]		= bin_dS[i]	      ;
      std::vector<double> _bin_dptggjj		(n_bin_dptggjj	        )	; for ( int i=0	; i< n_bin_dptggjj	 ; i++ ) _bin_dptggjj[i]	= bin_dptggjj[i]      ;

      _h_mgg_0j		= bookHisto1D(	"_h_mgg_0j",		_bin_mgg		);
      _h_ptgg_0j	= bookHisto1D(	"_h_ptgg_0j",		_bin_pTgg		);
      _h_dphigg_0j	= bookHisto1D(	"_h_dphigg_0j",		_bin_deltaphi		);
      _h_costhgg_0j	= bookHisto1D(	"_h_costhgg_0j",	_bin_costhetastar	);

      _h_mgg_1j		= bookHisto1D(	"_h_mgg_1j",		_bin_mgg		);
      _h_ptgg_1j	= bookHisto1D(	"_h_ptgg_1j",		_bin_pTgg		);
      _h_dphigg_1j	= bookHisto1D(	"_h_dphigg_1j",		_bin_deltaphi		);
      _h_costhgg_1j	= bookHisto1D(	"_h_costhgg_1j",	_bin_costhetastar	);
      _h_ptj1_1j	= bookHisto1D(	"_h_ptj1_1j",		_bin_ptj1		);
      _h_dRgj_1j_g1	= bookHisto1D(	"_h_dRgj_1j_g1",	_bin_dRgj		);
      _h_dRgj_1j_g2	= bookHisto1D(	"_h_dRgj_1j_g2",	_bin_dRgj		);

      _h_mgg_2j		= bookHisto1D(	"_h_mgg_2j",		_bin_mgg		);
      _h_ptgg_2j	= bookHisto1D(	"_h_ptgg_2j",		_bin_pTgg		);
      _h_dphigg_2j	= bookHisto1D(	"_h_dphigg_2j",		_bin_deltaphi		);
      _h_costhgg_2j	= bookHisto1D(	"_h_costhgg_2j",	_bin_costhetastar	);
      _h_mjj		= bookHisto1D(	"_h_mjj",		_bin_mass_jj		);
      _h_ptjj		= bookHisto1D(	"_h_ptjj",		_bin_pt_jj		);
      _h_dphijj		= bookHisto1D(	"_h_dphijj",		_bin_deltaphi_jj	);
      _h_dyjj		= bookHisto1D(	"_h_dyjj",		_bin_deltay_jj		);
      _h_ptj1_2j	= bookHisto1D(	"_h_ptj1_2j",		_bin_ptj1		);
      _h_ptj2_2j	= bookHisto1D(	"_h_ptj2_2j",		_bin_ptj2		);
      _h_dRgj_2j_g1j1	= bookHisto1D(	"_h_dRgj_2j_g1j1",	_bin_dRgj		);
      _h_dRgj_2j_g2j1	= bookHisto1D(	"_h_dRgj_2j_g2j1",	_bin_dRgj		);
      _h_dRgj_2j_g1j2	= bookHisto1D(	"_h_dRgj_2j_g1j2",	_bin_dRgj		);
      _h_dRgj_2j_g2j2	= bookHisto1D(	"_h_dRgj_2j_g2j2",	_bin_dRgj		);
      _h_dptggjj	= bookHisto1D(	"_h_dptggjj",		_bin_dptggjj		);
      _h_dSggjj		= bookHisto1D(	"_h_dSggjj",		_bin_dS			);
      _h_dSgjgj		= bookHisto1D(	"_h_dSgjgj",		_bin_dS			);

      _h_mgg_3j		= bookHisto1D(	"_h_mgg_3j",		_bin_mgg		);
      _h_ptgg_3j	= bookHisto1D(	"_h_ptgg_3j",		_bin_pTgg		);
      _h_dphigg_3j	= bookHisto1D(	"_h_dphigg_3j",		_bin_deltaphi		);
      _h_costhgg_3j	= bookHisto1D(	"_h_costhgg_3j",	_bin_costhetastar	);
      _h_ptj1_3j	= bookHisto1D(	"_h_ptj1_3j",		_bin_ptj1		);
      _h_ptj2_3j	= bookHisto1D(	"_h_ptj2_3j",		_bin_ptj2		);
      _h_ptj3_3j	= bookHisto1D(	"_h_ptj3_3j",		_bin_ptj2		);
     
    }

    // // @todo Prefer to use Rivet::binIndex()
    // size_t getEtaBin(double eta_w) const {
    //   const double aeta = fabs(eta_w);
    //   size_t v_iter = 0;
    //   for (; v_iter+1 < _eta_bins_areaoffset.size(); ++v_iter) {
    //     if (inRange(aeta, _eta_bins_areaoffset[v_iter], _eta_bins_areaoffset[v_iter+1])) break;
    //   }
    //   return v_iter;
    // }


    // Perform the per-event analysis
    void analyze(const Event& event) {

      const double weight = event.weight();

      // Require at least 2 photons in final state
      const Particles photons = applyProjection<IdentifiedFinalState>(event, "Photon").particlesByPt();
      if (photons.size() < 2) {
        vetoEvent;
      }

      // Compute the median energy density
      _ptDensity.clear();
      _sigma.clear();
      _Njets.clear();
      vector<vector<double> > ptDensities;
      vector<double> emptyVec;      
      ptDensities.assign(_eta_bins_areaoffset.size()-1, emptyVec);

      // Get jets, and corresponding jet areas
      const fastjet::ClusterSequenceArea* clust_seq_area = applyProjection<FastJets>(event, "KtJetsD05").clusterSeqArea();
      foreach (const fastjet::PseudoJet& jet, applyProjection<FastJets>(event, "KtJetsD05").pseudoJets(0.0*GeV)) {
	const double aeta = fabs(jet.eta());
	const double pt = jet.perp();
	const double area = clust_seq_area->area(jet);
	if (area < 1e-3) continue;
	const int ieta = binIndex(aeta, _eta_bins_areaoffset);
	if (ieta != -1) ptDensities[ieta].push_back(pt/area);
      }
      
      // Compute median jet properties over the jets in the event
      for (size_t b = 0; b < _eta_bins_areaoffset.size()-1; ++b) {
	double median = 0.0;
	double sigma = 0.0;
	int Njets = 0;
	if (ptDensities[b].size() > 0) {
	  std::sort(ptDensities[b].begin(), ptDensities[b].end());
	  int nDens = ptDensities[b].size();
	  median = (nDens % 2 == 0) ? (ptDensities[b][nDens/2]+ptDensities[b][(nDens-2)/2])/2 : ptDensities[b][(nDens-1)/2];
	  sigma = ptDensities[b][(int)(.15865*nDens)];
	  Njets = nDens;
	}
	_ptDensity.push_back(median);
	_sigma.push_back(sigma);
	_Njets.push_back(Njets);
      }

      // Loop over photons and fill vector of isolated ones
      Particles isolated_photons;
      foreach (const Particle& photon, photons) {
	// Check if it's a prompt photon (needed for SHERPA 2->5 sample, otherwise I also get photons from hadron decays in jets)
	if ( photon.fromDecay() ) continue;

	/// Remove photons in ECAL crack region
	if (inRange(photon.abseta(), 1.37, 1.52)) continue;
	const double eta_P = photon.eta();
	const double phi_P = photon.phi();
	//const double pt_P = photon.pt();
	//std::cout << "*** Eta = " << eta_P << " - Pt[GeV] = " << pt_P << std::endl;
	
	// Compute isolation via particles within an R=0.4 cone of the photon
	Particles fs = applyProjection<FinalState>(event, "FS").particles();
	FourMomentum mom_in_EtCone;
	foreach (const Particle& p, fs) {
	  // Reject if not in cone
	  if (deltaR(photon.momentum(), p.momentum()) > 0.4) continue;
	  // Reject if in the 5x7 cell central core
	  if (fabs(eta_P - p.eta()) < 0.025 * 5 * 0.5 &&
	      fabs(phi_P - p.phi()) < PI/128. * 7 * 0.5) continue;
	  // Sum momentum
	  mom_in_EtCone += p.momentum();
	}
	// Now figure out the correction (area*density)
	const double EtCone_area = PI*sqr(0.4) - (7*.025)*(5*PI/128.); // cone area - central core rectangle
	const double correction = _ptDensity[binIndex(fabs(eta_P), _eta_bins_areaoffset)] * EtCone_area;
	
	// Discard the photon if there is more than 9 GeV of cone activity
	// NOTE: Shouldn't need to subtract photon itself (it's in the central core)
	// NOTE: using expected cut at hadron/particle level, not at reco level
	if (mom_in_EtCone.Et() - correction > 9*GeV) continue;
	// Add isolated photon to list
	isolated_photons.push_back(photon);
      }

      // require at least two isolated photons
      if (isolated_photons.size() < 2) {
        vetoEvent;
      }

      // select leading pT pair
      sortByPt(isolated_photons);
      FourMomentum y1 = isolated_photons[0].momentum();
      FourMomentum y2 = isolated_photons[1].momentum();

      // Both leading and subleading photon should have pT > 22 GeV      
      if (y1.pT() < 22.*GeV) vetoEvent;
      if (y2.pT() < 22.*GeV) vetoEvent; // this should be be needed anymore since initial selection seems to work...

      // require the two photons to be separated (dR>0.4)
      if ( deltaR(y1,y2) < 0.4 ) {
       vetoEvent;
      }

      // Get the Jets 
      const Jets jets = applyProjection<FastJets>(event, "Jets").jetsByPt(25.0*GeV);
      Jets good_jets;
      foreach (const Jet& jet, jets) {
	const double absyjet = jet.absrap();
	const double ptjet = jet.pT();
	bool isJetAcceptance = false;
	if (absyjet < 2.4) {	  
	  if (ptjet > 25*GeV)
	    isJetAcceptance = true;
	} else
	if (absyjet > 2.4 && absyjet < 4.4) {	  
	  if (ptjet > 50*GeV)
	    isJetAcceptance = true;
	}
	if ( isJetAcceptance ) { // check separation from leading and subleading photons
	  if ( deltaR(y1.eta(),y1.phi(),jet.eta(),jet.phi()) > 0.6 &&
	       deltaR(y2.eta(),y2.phi(),jet.eta(),jet.phi()) > 0.6 )
	    good_jets.push_back(jet);
	}
      }
      int njet = good_jets.size();

      _h_njet->fill( njet, weight );

      // diphoton properties  
      FourMomentum gg		= y1+y2;
      const double Mgg		= gg.mass();
      const double pTgg		= gg.pT();
      const double dPhigg	= mapAngle0ToPi(y1.phi() - y2.phi());
      const double costhetagg	= 2 * y1.pT() * y2.pT() * sinh(y1.eta() - y2.eta()) / Mgg / add_quad(Mgg, pTgg);

      if (njet==0) {

	_h_mgg_0j	->fill( Mgg		, weight );	      
	_h_ptgg_0j	->fill( pTgg		, weight );      
	_h_dphigg_0j	->fill( dPhigg		, weight );    
	_h_costhgg_0j	->fill( costhetagg	, weight );	  	

      }

      if (njet==1) {
	
	FourMomentum j1 = good_jets[0];
	const double ptj1 = j1.pT();
	double dR_j1g1 = deltaR(y1.eta(),y1.phi(),j1.eta(),j1.phi());
	double dR_j1g2 = deltaR(y2.eta(),y2.phi(),j1.eta(),j1.phi());

	_h_mgg_1j	->fill( Mgg		, weight );	      
	_h_ptgg_1j	->fill( pTgg		, weight );      
	_h_dphigg_1j	->fill( dPhigg		, weight );    
	_h_costhgg_1j	->fill( costhetagg	, weight );	  	
	_h_ptj1_1j	->fill( ptj1		, weight ); 
	_h_dRgj_1j_g1	->fill( dR_j1g1		, weight ); 
	_h_dRgj_1j_g2	->fill( dR_j1g2		, weight ); 

      } 

      if (njet==2) {

	// dijet properties
	FourMomentum j1 = good_jets[0];
	FourMomentum j2 = good_jets[1];
	double ptj1	= j1.pT();
	double ptj2	= j2.pT();
	FourMomentum jj = j1+j2;
	double mjj	= jj.mass();
	double ptjj	= jj.pT();
	double dphijj	= fabs( j1.phi() - j2.phi() );
	double dyjj	= fabs( j1.rapidity() - j2.rapidity() );

	// gamma-jet properties
	double dR_j1g1 = deltaR(y1.eta(),y1.phi(),j1.eta(),j1.phi());
	double dR_j1g2 = deltaR(y2.eta(),y2.phi(),j1.eta(),j1.phi());
	double dR_j2g1 = deltaR(y1.eta(),y1.phi(),j2.eta(),j2.phi());
	double dR_j2g2 = deltaR(y2.eta(),y2.phi(),j2.eta(),j2.phi());
	
	// ggjj properties	
	FourMomentum g1j1 = y1 + j1;
	FourMomentum g2j2 = y2 + j2;
	double dptggjj	  = pTgg - ptjj;
	double dSggjj	  = fabs( gg.phi() - jj.phi() );
	double dSgjgj	  = fabs( g1j1.phi() - g2j2.phi() );

	_h_mgg_2j	->fill( Mgg		, weight );
	_h_ptgg_2j	->fill( pTgg		, weight );
	_h_dphigg_2j	->fill( dPhigg		, weight );
	_h_costhgg_2j	->fill( costhetagg	, weight );
	_h_ptj1_2j	->fill( ptj1		, weight ); 
	_h_ptj2_2j	->fill( ptj2		, weight ); 
	_h_mjj		->fill( mjj		, weight ); 
	_h_ptjj		->fill( ptjj		, weight ); 
	_h_dphijj	->fill( dphijj		, weight ); 
	_h_dyjj		->fill( dyjj		, weight ); 
	_h_dRgj_2j_g1j1 ->fill( dR_j1g1		, weight ); 
	_h_dRgj_2j_g2j1 ->fill( dR_j1g2		, weight ); 
	_h_dRgj_2j_g1j2 ->fill( dR_j2g1		, weight ); 
	_h_dRgj_2j_g2j2 ->fill( dR_j2g2		, weight ); 
	_h_dptggjj	->fill( dptggjj		, weight ); 
	_h_dSggjj	->fill( dSggjj		, weight ); 
	_h_dSgjgj	->fill( dSgjgj		, weight ); 

      }

      if (njet>=3) {

	FourMomentum j1 = good_jets[0];
	FourMomentum j2 = good_jets[1];
	FourMomentum j3 = good_jets[2];
	double ptj1	= j1.pT();
	double ptj2	= j2.pT();
	double ptj3	= j3.pT();

	_h_mgg_3j	->fill( Mgg		, weight );	      
	_h_ptgg_3j	->fill( pTgg		, weight );      
	_h_dphigg_3j	->fill( dPhigg		, weight );    
	_h_costhgg_3j	->fill( costhetagg	, weight );	  	
	_h_ptj1_3j	->fill( ptj1		, weight ); 
	_h_ptj2_3j	->fill( ptj2		, weight ); 
	_h_ptj3_3j	->fill( ptj3		, weight ); 
	
      }
    
    }

    // Normalise histograms etc., after the run
    void finalize() {

      scale( _h_njet	      	, crossSection()/sumOfWeights());

      scale( _h_mgg_0j	      	, crossSection()/sumOfWeights());
      scale( _h_ptgg_0j      	, crossSection()/sumOfWeights());
      scale( _h_dphigg_0j    	, crossSection()/sumOfWeights());
      scale( _h_costhgg_0j   	, crossSection()/sumOfWeights());

      scale( _h_mgg_1j	      	, crossSection()/sumOfWeights());
      scale( _h_ptgg_1j      	, crossSection()/sumOfWeights());
      scale( _h_dphigg_1j    	, crossSection()/sumOfWeights());
      scale( _h_costhgg_1j   	, crossSection()/sumOfWeights());
      scale( _h_ptj1_1j      	, crossSection()/sumOfWeights());
      scale( _h_dRgj_1j_g1   	, crossSection()/sumOfWeights());
      scale( _h_dRgj_1j_g2   	, crossSection()/sumOfWeights());

      scale( _h_mgg_2j	      	, crossSection()/sumOfWeights());
      scale( _h_ptgg_2j      	, crossSection()/sumOfWeights());
      scale( _h_dphigg_2j    	, crossSection()/sumOfWeights());
      scale( _h_costhgg_2j   	, crossSection()/sumOfWeights());
      scale( _h_mjj	      	, crossSection()/sumOfWeights());
      scale( _h_ptjj	      	, crossSection()/sumOfWeights());
      scale( _h_dphijj	      	, crossSection()/sumOfWeights());
      scale( _h_dyjj	      	, crossSection()/sumOfWeights());
      scale( _h_ptj1_2j      	, crossSection()/sumOfWeights());
      scale( _h_ptj2_2j      	, crossSection()/sumOfWeights());
      scale( _h_dRgj_2j_g1j1 	, crossSection()/sumOfWeights());
      scale( _h_dRgj_2j_g2j1 	, crossSection()/sumOfWeights());
      scale( _h_dRgj_2j_g1j2 	, crossSection()/sumOfWeights());
      scale( _h_dRgj_2j_g2j2 	, crossSection()/sumOfWeights());
      scale( _h_dptggjj      	, crossSection()/sumOfWeights());
      scale( _h_dSggjj	      	, crossSection()/sumOfWeights());
      scale( _h_dSgjgj	      	, crossSection()/sumOfWeights());

      scale( _h_mgg_3j	      	, crossSection()/sumOfWeights());
      scale( _h_ptgg_3j      	, crossSection()/sumOfWeights());
      scale( _h_dphigg_3j    	, crossSection()/sumOfWeights());
      scale( _h_costhgg_3j   	, crossSection()/sumOfWeights());
      scale( _h_ptj1_3j      	, crossSection()/sumOfWeights());
      scale( _h_ptj2_3j      	, crossSection()/sumOfWeights());
      scale( _h_ptj3_3j      	, crossSection()/sumOfWeights());

    }

  private:

    Histo1DPtr _h_njet	        ;

    Histo1DPtr _h_mgg_0j	;	      
    Histo1DPtr _h_ptgg_0j	;      
    Histo1DPtr _h_dphigg_0j	;    
    Histo1DPtr _h_costhgg_0j	;   

    Histo1DPtr _h_mgg_1j	;	      
    Histo1DPtr _h_ptgg_1j	;      
    Histo1DPtr _h_dphigg_1j	;    
    Histo1DPtr _h_costhgg_1j	;   
    Histo1DPtr _h_ptj1_1j	;      
    Histo1DPtr _h_dRgj_1j_g1	;   
    Histo1DPtr _h_dRgj_1j_g2	;   

    Histo1DPtr _h_mgg_2j	;	      
    Histo1DPtr _h_ptgg_2j	;  
    Histo1DPtr _h_dphigg_2j	;
    Histo1DPtr _h_costhgg_2j	;
    Histo1DPtr _h_mjj		;
    Histo1DPtr _h_ptjj		;
    Histo1DPtr _h_dphijj	;
    Histo1DPtr _h_dyjj		;
    Histo1DPtr _h_ptj1_2j	;
    Histo1DPtr _h_ptj2_2j	;
    Histo1DPtr _h_dRgj_2j_g1j1	;
    Histo1DPtr _h_dRgj_2j_g2j1	;
    Histo1DPtr _h_dRgj_2j_g1j2	;
    Histo1DPtr _h_dRgj_2j_g2j2	;
    Histo1DPtr _h_dptggjj	;
    Histo1DPtr _h_dSggjj	;
    Histo1DPtr _h_dSgjgj	;
    
    Histo1DPtr _h_mgg_3j	;
    Histo1DPtr _h_ptgg_3j	;
    Histo1DPtr _h_dphigg_3j	;
    Histo1DPtr _h_costhgg_3j	;
    Histo1DPtr _h_ptj1_3j	;
    Histo1DPtr _h_ptj2_3j	;
    Histo1DPtr _h_ptj3_3j	;

    fastjet::AreaDefinition* _area_def;

    std::vector<double> _eta_bins;
    std::vector<double> _eta_bins_areaoffset; 
    std::vector<double> _ptDensity;
    std::vector<double> _sigma;
    std::vector<double> _Njets;
    
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAS_2012_DIPHOTONS_JETS);

}


