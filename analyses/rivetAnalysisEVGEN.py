theApp.EvtMax = -1 ## all events in sample
# theApp.EvtMax = 1000

import AthenaPoolCnvSvc.ReadAthenaPool
svcMgr.EventSelector.InputCollections = ["EVNT.pool.root"]

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()

# analysis run form current directory
import os
rivet.AnalysisPath = os.environ['PWD']

## DIPHOTON 2012
#rivet.Analyses += [ 'ATLAS_2012_DIPHOTONS' ]

## DIPHOTON + JETS 2012
rivet.Analyses += [ 'ATLAS_2012_DIPHOTONS_JETS' ]

# Specify run name for histograms (leave empty if you don't know)
rivet.RunName = ""
# Specify AIDA output filename which will contain the histograms (without extension)
#rivet.HistoFile = "ATLAS_2012_DIPHOTONS"
rivet.HistoFile = "ATLAS_2012_DIPHOTONS_JETS"

# Specify MC cross section in pb (AMI*1e3) if necessary for your analysis

## PYTHIA 2DP20 MC12
## mc12_8TeV.129180.Pythia8_AU2CTEQ6L1_gammagamma_2DP20.evgen.EVNT.e1199/
#rivet.CrossSection = 138.09e3*0.000648 # Cross section * filter efficiency

## SHERPA 2DP20 3j_Myy55to80GeV
## mc12_8TeV.181766.Sherpa_CT10_2DP20_3j_Myy55to80GeV.evgen.EVNT.e2512/
#rivet.CrossSection = 914.56e3*0.34037

## SHERPA 2DP20 3j_Myy80GeV
## mc12_8TeV:mc12_8TeV.146824.Sherpa_CT10_2DP20_3j_Myy80GeV.evgen.EVNT.e1434/
rivet.CrossSection = 616.35e3 * 0.39305
 
topSequence += rivet

from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()
ServiceMgr.THistSvc.Output = ["Rivet DATAFILE='Rivet.root' OPT='RECREATE'"]
