include="--extFile=Rivet_ATLAS_2012_DIPHOTONS.so,ATLAS_2012_DIPHOTONS.aida,Rivet_ATLAS_2012_DIPHOTONS_JETS.so,ATLAS_2012_DIPHOTONS_JETS.aida"
script="rivetAnalysisEVGEN.py"
outfile="Rivet.root"
sitename="--site=AUTO"

### PYTHIA 2DP20 ###

## DIPHOTONS

## datasetname="mc12_8TeV.129180.Pythia8_AU2CTEQ6L1_gammagamma_2DP20.evgen.EVNT.e1199/"
## output="user.mdelmast.mc12_8TeV.129180.Pythia8_AU2CTEQ6L1_gammagamma_2DP20.Rivet.Diphoton2012.v05"

## datasetname="user.mdelmast.mc12_8TeV.129180.Pythia8_AU2CTEQ6L1_gammagamma_2DP20_NoHadUE.evgen.EVNT.v03_EXT0/"
## output="user.mdelmast.mc12_8TeV.129180.Pythia8_AU2CTEQ6L1_gammagamma_2DP20_NoHadUE.Rivet.Diphoton2012.v01"

## datasetname="user.mdelmast.mc12_8TeV.129180.Pythia8_AU2CTEQ6L1_gammagamma_2DP20_NoHadUE.evgen.EVNT.v04_EXT0/"
## output="user.mdelmast.mc12_8TeV.129180.Pythia8_AU2CTEQ6L1_gammagamma_2DP20_NoHadUE.Rivet.Diphoton2012.v02"

## DIPHOTONS + JETS

#datasetname="mc12_8TeV.129180.Pythia8_AU2CTEQ6L1_gammagamma_2DP20.evgen.EVNT.e1199/"
#output="user.mdelmast.mc12_8TeV.129180.Pythia8_AU2CTEQ6L1_gammagamma_2DP20.Rivet.DiphotonJets2012.v04"

#datasetname="user.mdelmast.mc12_8TeV.129180.Pythia8_AU2CTEQ6L1_gammagamma_2DP20_NoHadUE.evgen.EVNT.v04_EXT0/"
#output="user.mdelmast.mc12_8TeV.129180.Pythia8_AU2CTEQ6L1_gammagamma_2DP20_NoHadUE.Rivet.DiphotonJets2012.v04"

### SHERPA 2DP20 3j_Myy55to80GeV ###

## DIPHOTONS

## datasetname="mc12_8TeV.181766.Sherpa_CT10_2DP20_3j_Myy55to80GeV.evgen.EVNT.e2512/"
## output="user.mdelmast.mc12_8TeV.181766.Sherpa_CT10_2DP20_3j_Myy55to80GeV.Rivet.Diphoton2012.v01"

## datasetname="user.mdelmast.mc12_8TeV.181766.Sherpa_CT10_2DP20_3j_Myy55to80GeV_NoHadUE.EVNT.v02_EXT0/"
## output="user.mdelmast.mc12_8TeV.181766.Sherpa_CT10_2DP20_3j_Myy55to80GeV_NoHadUE.Rivet.Diphoton2012.v04"

## DIPHOTONS + JETS

#datasetname="mc12_8TeV.181766.Sherpa_CT10_2DP20_3j_Myy55to80GeV.evgen.EVNT.e2512/"
#output="user.mdelmast.mc12_8TeV.181766.Sherpa_CT10_2DP20_3j_Myy55to80GeV.Rivet.DiphotonJets2012.v04"

#datasetname="user.mdelmast.mc12_8TeV.181766.Sherpa_CT10_2DP20_3j_Myy55to80GeV_NoHadUE.EVNT.v02_EXT0/"
#output="user.mdelmast.mc12_8TeV.181766.Sherpa_CT10_2DP20_3j_Myy55to80GeV_NoHadUE.Rivet.DiphotonJets2012.v06"

### SHERPA 2DP20 3j_80GeV ###

## DIPHOTONS

## datasetname="mc12_8TeV.146824.Sherpa_CT10_2DP20_3j_Myy80GeV.evgen.EVNT.e1434/"
## output="user.mdelmast.mc12_8TeV.146824.Sherpa_CT10_2DP20_3j_Myy80GeV.Rivet.Diphoton2012.v01"

## datasetname="user.mdelmast.mc12_8TeV.146824.Sherpa_CT10_2DP20_3j_Myy80GeV_NoHadUE.EVNT.v02_EXT0/"
## output="user.mdelmast.mc12_8TeV.146824.Sherpa_CT10_2DP20_3j_Myy80GeV_NoHadUE.Rivet.Diphoton2012.v03"

## DIPHOTONS + JETS

#datasetname="mc12_8TeV.146824.Sherpa_CT10_2DP20_3j_Myy80GeV.evgen.EVNT.e1434/"
#output="user.mdelmast.mc12_8TeV.146824.Sherpa_CT10_2DP20_3j_Myy80GeV.Rivet.DiphotonJets2012.v04"

datasetname="user.mdelmast.mc12_8TeV.146824.Sherpa_CT10_2DP20_3j_Myy80GeV_NoHadUE.EVNT.v02_EXT0/"
output="user.mdelmast.mc12_8TeV.146824.Sherpa_CT10_2DP20_3j_Myy80GeV_NoHadUE.Rivet.DiphotonJets2012.v06"

nfilesperjob=200

echo "*** Submitting $script on $datasetname ..."

#pathena --long --inDS=$datasetname --outDS=$output --nFilesPerJob $nfilesperjob $include $script
pathena --inDS=$datasetname --outDS=$output --nFilesPerJob $nfilesperjob $include $script
